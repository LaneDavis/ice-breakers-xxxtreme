﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCloud : MonoBehaviour {

    public List<Sprite> CloudSprites = new List<Sprite>();
    public SpriteRenderer SpriteRenderer;
    public int OrderMin;
    public int OrderMax;
    public float BaseScale;
    public float OpacMin;
    public float OpacMax;
    public float BaseSpeed;
    public float Speed => BaseSpeed / dist;
    public float DistMin;
    public float DistMax;
    private float dist;
    private float Dist => (dist - DistMin) / (DistMax / DistMin);

    private void Start() {
        dist = Random.Range(DistMin, DistMax);
        transform.localScale = new Vector3(BaseScale / dist, BaseScale / dist, BaseScale / dist);
        SpriteRenderer.color = new Color(1f, 1f, 1f, Mathf.Lerp(OpacMax, OpacMin, Dist));
        SpriteRenderer.sortingOrder = (int) Mathf.Lerp(OrderMax, OrderMin, Dist);
        SpriteRenderer.sprite = CloudSprites.RandomElement();
    }

    private void Update() {
        transform.Translate(-1f * Speed * Time.deltaTime, 0f, 0f);
    }

}
