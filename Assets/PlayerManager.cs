﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

using Rewired;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Instance;

    public List<GameObject> LevelPrefabs;
    public List<Team> Teams;
    public static List<SavedTeam> savedTeams = new List<SavedTeam>();
    private List<Player> players = new List<Player>();
    [HideInInspector] public List<Player> playersThatExist = new List<Player>();
    [HideInInspector] public List<Player> playersDoingSelection = new List<Player>();
    static Sprite[] HatChoices = new Sprite[4];
    public GameObject PlayerPrefab;
    public float ResetHoldTime = 3f;
    public SoundCall PlayerDeathSound;
    private float resetTimer;
    
    [Header("Player Selection")]
    public List<PlayerSelectionDisplayer> SelectionDisplayers = new List<PlayerSelectionDisplayer>();
    
    [Header("Game Start")]
    public Animator StartScreenAnimator;
    public SpriteRenderer StartScreenBackground;
    public AnimationCurve StartScreenBackgroundOpac;
    private float backgroundTransitionTimer;
    public PostProcessVolume BloomVolume;
    public AnimationCurve BloomSteadyLoop;
    public AnimationCurve BloomTransition;
    private float backgroundSteadyTimer;
    private float bloomSteadyLoopLastValue;
    private float bloomSteadyLoopLastValueVelocity;
    public float BloomFadeSpeed = 5f;

    [Header("Game Reset")]
    public TextMeshProUGUI ResetText;
    public RectTransform ResetBarRT;
    public Image ResetBarImage;
    private float ResetProgress => resetTimer / ResetHoldTime;
    public AnimationCurve ResetTextOpacCurve;
    public AnimationCurve ResetBarOpacCurve;

    private bool MoreThanOneTeamHasBeenAlive {
        get {
            int num = 0;
            foreach (Team team in Teams)
            {
                if (team.TeamHasBeenAlive) num++;
            }
            return num > 1;
        }
    }

    private int NumLivingTeams {
        get
        {
            int num = 0;
            foreach (Team team in Teams)
            {
                if (team.TeamHasBeenAlive && !team.TeamIsDead) num++;
            }
            return num;
        }
    }

    private void Start() {
        Instance = this;

        if (savedTeams.Count <= 0) {
            for (int i = 0; i < 4; i++)
            {
                savedTeams.Add(new SavedTeam());
            }
        } else {
            Destroy(StartScreenBackground);
            Destroy(StartScreenAnimator.gameObject);
        }

        for (int i = 0; i < 4; i++)
        {
            players.Add(ReInput.players.GetPlayer(i));
        }

        // Pick a Random Level
        int randomLevel = UnityEngine.Random.Range(0, LevelPrefabs.Count);
        GameObject newLevel = Instantiate(LevelPrefabs[randomLevel]);
        newLevel.GetComponent<LevelController>().SpawnPoints = Fisher_Yates_CardDeck_Shuffle(newLevel.GetComponent<LevelController>().SpawnPoints);
        for (int i = 0; i < 4; i++)
        {
            Teams[i].SpawnPoint = newLevel.GetComponent<LevelController>().SpawnPoints[i];
        }

        // Pull Data from Saved Teams
        for (int i = 0; i < 4; i++){
            Teams[i].TeamScore = savedTeams[i].score;
        }
        
        // Create Pre-Existing Players
        for (int i = 0; i < 4; i++){
            foreach (int playerNum in savedTeams[i].players) {
                CreatePlayer(playerNum, i, HatChoices[playerNum]);
            }
        }

        foreach (Team team in Teams){
            team.ScoreText.text = team.ScoreString;
            team.TeamLogoImage.sprite = team.Data.LogoSprite;
            team.TeamLogoImage.color = team.Data.IconColor;
        }

        // Load Audio Scene
        if (BGMPlayer.Instance == null) {
            Debug.Log("reload");
            SceneManager.LoadScene("Background Music", LoadSceneMode.Additive);
        }

    }

    // Update is called once per frame
    void Update () {

//        backgroundSteadyTimer += Time.deltaTime;
//        float bloomTransition = 0f;
//        if (playersThatExist.Count == 0 && StartScreenBackground != null) {
//            bloomSteadyLoopLastValue = BloomSteadyLoop.Evaluate(backgroundSteadyTimer);
//        }
//        else {
//            bloomSteadyLoopLastValue = Mathf.SmoothDamp(bloomSteadyLoopLastValue, 0f, ref bloomSteadyLoopLastValueVelocity, 1f / BloomFadeSpeed);
//        }
//        
//        if (playersThatExist.Count > 0 && StartScreenBackground != null) {
//            backgroundTransitionTimer += Time.deltaTime;
//            bloomTransition = BloomTransition.Evaluate(backgroundTransitionTimer);
//            StartScreenBackground.color = new Color(1f, 1f, 1f, StartScreenBackgroundOpac.Evaluate(backgroundTransitionTimer));
//        }
//
//        BloomVolume.weight = Mathf.Max(bloomTransition, bloomSteadyLoopLastValue);

        bool anyPlayerTriedToRestart = false;
        for (int i = 0; i < 4; i++)
        {
            // Remove Existing Player
            if (playersThatExist.Contains(players[i]))
            {
                if (players[i].GetButtonDown("RemovePlayerA") || players[i].GetButtonDown("RemovePlayerB")) {
                    DeletePlayer(i);
                }
            }

            // Add New Player
            if (!playersThatExist.Contains(players[i]) && !playersDoingSelection.Contains(players[i])) {
                if (players[i].GetButtonDown("RemovePlayerA") || players[i].GetButtonDown("RemovePlayerB") || players[i].GetButtonDown("Confirm")) {
                    SelectionDisplayers[i].Init(i);
                    playersDoingSelection.Add(players[i]);
                }
//                bool teamJoined = false;
//                for (int j = 0; j < Teams.Count; j++)
//                {
//                    if (players[i].GetButtonDown("JoinTeam" + j))
//                    {
//                        CreatePlayer(i, j);
//                        teamJoined = true;
//                    }
//                }
//
//                // Pressing Start joins the next unoccupied team.
//                if (!teamJoined)
//                {
//                    if (players[i].GetButtonDown("RemovePlayerA") || players[i].GetButtonDown("RemovePlayerB")) {
//                        for (int j = 0; j < Teams.Count; j++) {
//                            if (Teams[j].PlayerControllers.Count == 0) {
//                                CreatePlayer(i, j);
//                                break;
//                            }
//                        }
//                    }
//                }
            }

            if (players[i].GetButton("RemovePlayerA") && players[i].GetButton("RemovePlayerB"))
            {
                anyPlayerTriedToRestart = true;
            }
        }

        if (anyPlayerTriedToRestart) {
            resetTimer += Time.deltaTime;
            ResetText.color = new Color(1f, 1f, 1f, ResetTextOpacCurve.Evaluate(ResetProgress));
            ResetBarImage.color = new Color(1f, 1f, 1f, ResetBarOpacCurve.Evaluate(ResetProgress));
            ResetBarRT.sizeDelta = new Vector2(Screen.width * ResetProgress, ResetBarRT.sizeDelta.y);
            if (resetTimer > ResetHoldTime) {
                HardReset();
            }
        } else {
            ResetText.color = new Color(1f, 1f, 1f, 0f);
            ResetBarImage.color = new Color(1f, 1f, 1f, 0f);
            ResetBarRT.sizeDelta = new Vector2(0f, ResetBarRT.sizeDelta.y);
            resetTimer = 0f;
        }

    }

    public void CreatePlayer (int playerNum, int teamNum, Sprite hatSprite) {
        playersDoingSelection.Remove(players[playerNum]);
        HatChoices[playerNum] = hatSprite;
        if (playersThatExist.Count == 0 && StartScreenAnimator != null) {
            StartScreenAnimator.Play("Fade Out");
        }
        GameObject newPlayer = Instantiate(PlayerPrefab);
        PlayerController newController = newPlayer.GetComponent<PlayerController>();
        newController.StandSprite = Teams[teamNum].Data.StandSprite;
        newController.JumpSprite = Teams[teamNum].Data.JumpSprite;
        newController.BodySprite.sprite = Teams[teamNum].Data.JumpSprite;
        newPlayer.transform.position = Teams[teamNum].SpawnPoint.transform.position;
        Teams[teamNum].PlayerControllers.Add(newController);
        newController.PlayerNum = playerNum;
        newController.SetTeamColor(Teams[teamNum].Data.GlowColor);
        playersThatExist.Add(players[playerNum]);
        Teams[teamNum].TeamHasBeenAlive = true;
        if (!savedTeams[teamNum].players.Contains(playerNum)) {
            savedTeams[teamNum].players.Add(playerNum);
        }
        newController.SetHat(hatSprite);
    }

    public void CancelSelectionForPlayer(int playerNum) {
        playersDoingSelection.Remove(players[playerNum]);
    }
    
    void DeletePlayer (int playerNum) {
        int teamNum = TeamNumOfPlayerNum(playerNum);
        if (teamNum == -1) return;
        if (ObjectOfPlayerNum(playerNum) != null) {
            ReportPlayerDeath(ObjectOfPlayerNum(playerNum));
        }
        savedTeams[teamNum].players.Remove(playerNum);
    }

    public void ReportPlayerDeath (GameObject g) {
        CamshakeManager.ApplyTrauma(0.5f);
        TeamOfPlayerObject(g).DeadPlayerControllers.Add(g.GetComponent<PlayerController>());
        SoundManager.Instance.PlaySound(PlayerDeathSound, g);
        Destroy(g);
        if (NumLivingTeams <= 1)
        {
            if (MoreThanOneTeamHasBeenAlive)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (!Teams[i].TeamIsDead)
                    {
                        savedTeams[i].score++;
                    }
                }
            }
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void HardReset () {
        savedTeams.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private Team TeamOfPlayerObject (GameObject playerObject) {
        foreach (Team t in Teams) {
            if (t.PlayerControllers.Contains(playerObject.GetComponent<PlayerController>())) {
                return t;
            }
        }
        return null;
    }

    public bool PlayerOnWinningTeam(PlayerController playerController) {
        Team t = Teams[TeamNumOfPlayerNum(playerController.PlayerNum)];
        return TeamHasHighestScore(t);
    }

    private GameObject ObjectOfPlayerNum (int PlayerNum) {
        for (int i = 0; i < Teams.Count; i++) {
            foreach(PlayerController pc in Teams[i].PlayerControllers) {
                if (pc != null && pc.PlayerNum == PlayerNum) {
                    return pc.gameObject;
                }
            }
        }
        return null;
    }

    private int TeamNumOfPlayerNum (int playerNum) {
        for (int i = 0; i < 4; i++) {
            if (savedTeams[i].players.Contains(playerNum)) {
                return i;
            }
        }
        return -1;
    }

    private bool TeamHasHighestScore (Team team) {
        return !Teams.Any(e => e != team && e.TeamScore >= team.TeamScore);
    }

    public static List<GameObject> Fisher_Yates_CardDeck_Shuffle(List<GameObject> aList)
    {

        System.Random _random = new System.Random();

        GameObject myGO;

        int n = aList.Count;
        for (int i = 0; i < n; i++)
        {
            // NextDouble returns a random number between 0 and 1.
            // ... It is equivalent to Math.random() in Java.
            int r = i + (int)(_random.NextDouble() * (n - i));
            myGO = aList[r];
            aList[r] = aList[i];
            aList[i] = myGO;
        }

        return aList;
    }

}
[System.Serializable]
public class Team {
    
    public TeamData Data;
    public TextMeshProUGUI ScoreText;
    public Image TeamLogoImage;
    public List<PlayerController> PlayerControllers;
    [HideInInspector] public List<PlayerController> DeadPlayerControllers = new List<PlayerController>();
    public GameObject SpawnPoint;
    public int TeamScore;

    [HideInInspector] public bool TeamHasBeenAlive;
    public bool TeamIsDead {
        get {
            return NumLivingPlayerControllers <= 0;
        }
    }
    public string ScoreString{
        get{
            return TeamScore.ToString();
        }
    }
    private int NumLivingPlayerControllers {
        get {
            int num = 0;
            foreach (PlayerController pc in PlayerControllers) {
                if (pc != null && !DeadPlayerControllers.Contains(pc)) num++;
            }
            return num;
        }
    }
}
public class SavedTeam{
    public int score;
    public List<int> players = new List<int>();
}