﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class StarTrailMod : MonoBehaviour {

    public float TimeMultiplier = 0.002f;
    public float WidthMultiplier = 0.0001f;
    public float RatioMultiplier = 0.0001f;
    private ParticleSystem particleSystem;

    private float curWidthPreCurve;
    public AnimationCurve WidthCurve;
    private float widthCurveTimer;
    
    private void Start() {
        particleSystem = GetComponent<ParticleSystem>();
        curWidthPreCurve = particleSystem.trails.widthOverTrail.constantMax;
    }

    private void Update() {
        var particleSystemTrails = particleSystem.trails;
        var minMaxCurve = particleSystemTrails.lifetime;
        minMaxCurve.constantMax += TimeMultiplier * Time.deltaTime;
        particleSystemTrails.lifetime = minMaxCurve;

        
        curWidthPreCurve += WidthMultiplier * Time.deltaTime;
        widthCurveTimer += Time.deltaTime;
        var widthOverTrail = particleSystemTrails.widthOverTrail;
        widthOverTrail.constantMax = curWidthPreCurve * WidthCurve.Evaluate(widthCurveTimer);
        particleSystemTrails.widthOverTrail = widthOverTrail;

        particleSystemTrails.ratio += RatioMultiplier * Time.deltaTime;
    }

}
