﻿using UnityEngine;

public class SplashBox : MonoBehaviour {

    public float SplashWidthMultiplier;
    public float SplashHeightMultiplier;
    
    // Die Die Die
    void OnTriggerEnter2D(Collider2D other) {
        Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
        if (rb == null) return;
        float velocity = rb.velocity.magnitude;
        float mass = rb.mass;
        float force = rb.mass * velocity;
        WaterHolder.WaterSim.GenerateRippleAtPosition(other.transform.position, force * SplashWidthMultiplier, -force * SplashHeightMultiplier, false);
    }

}
