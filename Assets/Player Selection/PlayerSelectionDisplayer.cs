﻿using System.Collections.Generic;
using Rewired;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.UI;

public class PlayerSelectionDisplayer : MonoBehaviour {

    private int playerIdx;
    private Player player;
    private bool selectionComplete = false;

    public GameObject DisplayPanel;
    
    public Image PenguinImage;
    public Image HatImage;
    public Image LogoImage;
    public Image LeftArrowBaseImage;
    public Image LeftArrowBrightImage;
    public Image RightArrowBaseImage;
    public Image RightArrowBrightImage;

    public List<TeamData> Teams = new List<TeamData>();

    public HatData Hats;
    
    // Cooldowns
    public float InputCooldown = 0.2f;
    private float nextInputTime = 0f;
    
    // Selection Data
    private int hatIdx = -1;
    private int teamIdx = -1;
    
    
    public void Init(int playerNum) {
        playerIdx = playerNum;
        player = ReInput.players.GetPlayer(playerNum);
        selectionComplete = false;
        hatIdx = Random.Range(0, Hats.HatSprites.Count);
        teamIdx = Random.Range(0, Teams.Count);
        UpdateHat();
        UpdateTeam();
        nextInputTime = Time.time + InputCooldown;
        DisplayPanel.SetActive(true);
    }

    public void Update() {

        if (player == null || selectionComplete) return;

        UpdateInput();

    }

    private void UpdateInput() {
        
        // Hat Left
        if (Time.time > nextInputTime && player.GetButton("Jump")) {
            nextInputTime = Time.time + InputCooldown;
            hatIdx = hatIdx == 0 ? Hats.HatSprites.Count - 1 : hatIdx - 1;
            UpdateHat();
        }
        
        // Hat Right
        if (Time.time > nextInputTime && player.GetButton("Throw")) {
            nextInputTime = Time.time + InputCooldown;
            hatIdx = hatIdx == Hats.HatSprites.Count - 1 ? 0 : hatIdx + 1;
            UpdateHat();
        }
        
        // Navigate Left
        if (Time.time > nextInputTime && player.GetAxisRaw("MoveX") < -0.3f) {
            nextInputTime = Time.time + InputCooldown;
            teamIdx = teamIdx == 0 ? Teams.Count - 1 : teamIdx - 1;
            UpdateTeam();
        }
        
        // Navigate Right
        if (Time.time > nextInputTime && player.GetAxisRaw("MoveX") > 0.3f) {
            nextInputTime = Time.time + InputCooldown;
            teamIdx = teamIdx == Teams.Count - 1 ? 0 : teamIdx + 1;
            UpdateTeam();
        }
        
        // Enter Game
        if (Time.time > nextInputTime && (player.GetButtonDown("Confirm") || player.GetButtonDown("RemovePlayerA") || player.GetButtonDown("RemovePlayerB"))) {
            nextInputTime = Time.time + InputCooldown;
            PlayerManager.Instance.CreatePlayer(playerIdx, teamIdx, Hats.HatSprites[hatIdx]);
            selectionComplete = true;
            DisplayPanel.SetActive(false);
        }

        // Cancel Selection
        if (Time.time > nextInputTime && player.GetButtonDown("Cancel")) {
            nextInputTime = Time.time + InputCooldown;
            PlayerManager.Instance.CancelSelectionForPlayer(playerIdx);
            player = null;
            DisplayPanel.SetActive(false);
        }

    }

    private void UpdateHat() {
        HatImage.sprite = Hats.HatSprites[hatIdx];

        GameObject newHatImage = Instantiate(HatImage.gameObject, HatImage.transform.position, HatImage.transform.rotation, HatImage.transform.parent);
        Destroy(HatImage.gameObject);
        HatImage = newHatImage.GetComponent<Image>();
        newHatImage.transform.SetAsLastSibling();
    }

    private void UpdateTeam() {
        LogoImage.sprite = Teams[teamIdx].LogoSprite;
        LogoImage.color = Teams[teamIdx].IconColor;
        PenguinImage.sprite = Teams[teamIdx].StandSprite;
        LeftArrowBaseImage.color = Teams[teamIdx].ArrowColorBase;
        LeftArrowBrightImage.color = Teams[teamIdx].ArrowColorBright;
        RightArrowBaseImage.color = Teams[teamIdx].ArrowColorBase;
        RightArrowBrightImage.color = Teams[teamIdx].ArrowColorBright;

        GameObject newLogoImage = Instantiate(LogoImage.gameObject, LogoImage.transform.position, LogoImage.transform.rotation, LogoImage.transform.parent);
        Destroy(LogoImage.gameObject);
        LogoImage = newLogoImage.GetComponent<Image>();
        newLogoImage.transform.SetAsFirstSibling();
    }

}
