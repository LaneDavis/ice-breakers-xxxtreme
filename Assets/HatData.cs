﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Hat Data", menuName = "Hats/HatData", order = 1)]
public class HatData : ScriptableObject {
    
    public List<Sprite> HatSprites = new List<Sprite>();
    
}
