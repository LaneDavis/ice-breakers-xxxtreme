﻿using UnityEngine;

public class ShadowCaster : MonoBehaviour {

    private Camera mainCamera;
    private Transform trackedTransform;
    private float sideMultiplier = 1f;
    
    public void SetTrackedObject(Transform objectTransform, float dirMultiplier) {
        trackedTransform = objectTransform;
        mainCamera = Camera.main;
        sideMultiplier = dirMultiplier;

        MatchTransform();
    }

    private void Update() {

        if (trackedTransform == null) {
            Destroy(gameObject);
            return;
        }

        MatchTransform();

    }

    private void MatchTransform() {
        float mag = 2.0f * mainCamera.ScreenToWorldPoint(new Vector3( Screen.width,  Screen.height, 1f)).x;
        transform.position = (Vector2)trackedTransform.transform.position + Vector2.right * mag * sideMultiplier;
        transform.localScale = trackedTransform.localScale;
        transform.rotation = trackedTransform.rotation;
    }
    
}
