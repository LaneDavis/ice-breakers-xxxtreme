﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeCharacterAnimations : MonoBehaviour {

	[Header("Walking")]
	public FxPackageController FxLeft;
	public FxPackageController FxRight;

	public float walkPowerMult;
	public float walkThreshold;
	public float timePerPulse;
	private float unspentTime;
	private bool nextIsRight = true;

	[Header("Jumping")] 
	public FxPackageController FxJump;
	public FxPackageController FxDoubleJumpRight;
	public FxPackageController FxDoubleJumpLeft;
	public SoundCall JumpSound;
	private const float JumpSoundCooldown = 0.25f;
	private float timeOfNextJumpSound = 0f;

	[Header("Throwing")]
	public FxPackageController FxHoldStart;
	public FxPackageController FxHoldToggle;

	public void WalkAnimation(float horizontalMove) {
		if (Mathf.Abs(horizontalMove) < walkThreshold) return;
		unspentTime += Time.deltaTime;
		if (unspentTime > timePerPulse) {
			unspentTime = 0f;
			if (nextIsRight) {
				FxRight.Trigger(null, Mathf.Sqrt(Mathf.Abs(horizontalMove)) * Mathf.Abs(walkPowerMult));
			} else {
				FxLeft.Trigger(null,  Mathf.Sqrt(Mathf.Abs(horizontalMove)) * Mathf.Abs(walkPowerMult));
			}

			nextIsRight = !nextIsRight;
		}
 	}

	public void JumpAnimation() {
		FxJump.Trigger();
		if (Time.time > timeOfNextJumpSound) {
			timeOfNextJumpSound = Time.time + JumpSoundCooldown;
			if (JumpSound != null) SoundManager.Instance.PlaySound(JumpSound, gameObject);
		}
	}

	public void DoubleJumpRightAnimation() {
		FxDoubleJumpRight.Trigger();
		if (Time.time > timeOfNextJumpSound) {
			timeOfNextJumpSound = Time.time + JumpSoundCooldown;
			if (JumpSound != null) SoundManager.Instance.PlaySound(JumpSound, gameObject);
		}
	}

	public void DoubleJumpLeftAnimation() {
		FxDoubleJumpLeft.Trigger();
		if (Time.time > timeOfNextJumpSound) {
			timeOfNextJumpSound = Time.time + JumpSoundCooldown;
			if (JumpSound != null) SoundManager.Instance.PlaySound(JumpSound, gameObject);
		}
	}

	public void HoldStart() {
		FxHoldStart.Trigger();
	}
	
}
