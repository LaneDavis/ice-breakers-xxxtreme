﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillboxController : MonoBehaviour {

    public GameObject PlayerDeathBurst;
    public SoundCall SnowballDropSound;

    public GameObject SplashPrefab;

    // Die Die Die
    void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.name.Contains("ichard")) {
//            PlayerController pc = collision.gameObject.GetComponent<PlayerController>();
//            GameObject splash = Instantiate(SplashPrefab, collision.transform.position, Quaternion.identity, null);
//            foreach (ParticleSystem ps in splash.GetComponentsInChildren<ParticleSystem>()) {
//                var mainModule = ps.main;
//                var mainModuleStartColor = mainModule.startColor;
//                mainModuleStartColor.color = pc.TeamColor;
//            }
            Instantiate(PlayerDeathBurst, collision.transform.position, Quaternion.identity, null);
            PlayerManager.Instance.ReportPlayerDeath(collision.gameObject);
        } else {
            SoundManager.Instance.PlaySound(SnowballDropSound, collision.gameObject);
            Destroy(collision.gameObject);
        }
    }

}
