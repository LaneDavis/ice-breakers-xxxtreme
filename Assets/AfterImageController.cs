﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AfterImageController : MonoBehaviour {

    public Sprite StandSprite;
    public Sprite JumpSprite;

    public SpriteRenderer Renderer;

    public AnimationCurve OpacityByTime;
    public AnimationCurve ScaleByTime;
    private float timer;
    public float Duration;

    private Vector3 baseScale;

    public void SetScale(Vector3 scale) {
        baseScale = scale;
        transform.localScale = scale;
    }
    
    public void InitStanding(Color teamColor) {
        Renderer.sprite = StandSprite;
        Renderer.color = teamColor;
    }

    public void InitJumping(Color teamColor) {
        Renderer.sprite = JumpSprite;
        Renderer.color = teamColor;
    }

    private void Update() {
        timer += Time.deltaTime;
        Renderer.color = new Color(Renderer.color.r, Renderer.color.g, Renderer.color.b, OpacityByTime.Evaluate(timer / Duration));
        transform.localScale = new Vector3(baseScale.x * ScaleByTime.Evaluate(timer / Duration), baseScale.y * ScaleByTime.Evaluate(timer / Duration), baseScale.z * ScaleByTime.Evaluate(timer / Duration));
        if (timer >= Duration) {
            Destroy(gameObject);
        }
    }

}
