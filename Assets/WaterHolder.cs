﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Water2DTool;

public class WaterHolder : MonoBehaviour {

    public static WaterHolder Instance;
    public static Water2D_Simulation WaterSim;
    
    private void Awake() {
        Instance = this;
        WaterSim = GetComponent<Water2D_Simulation>();
    }

}
