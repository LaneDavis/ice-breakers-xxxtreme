﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

    public List<GameObject> SpawnPoints;
    public List<GameObject> ObjectsToBlowUp;

    public void Update()
    {
        foreach (GameObject go in ObjectsToBlowUp)
        {
            Instantiate(go, go.transform.position, go.transform.rotation, go.transform.parent);
            Destroy(go);
        }
    }

}
