﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Water2DTool;

public class SnowballController : MonoBehaviour {

    public GameObject particle;
    public GameObject burstprefab; 

    private void OnCollisionEnter2D(Collision2D collision) {
        particle.transform.SetParent(null);
        Instantiate(burstprefab, transform.position, Quaternion.identity, null);
        Destroy(gameObject);
        foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>()) {
	        ps.transform.SetParent(null);
	        ThreadingUtil.Instance.RunLater(ps.GetComponent<DestroySelf>().DoIt, 2f);
        }
    }
}


