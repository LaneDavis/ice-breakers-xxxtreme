﻿using UnityEngine;

[CreateAssetMenu(fileName = "TeamData", menuName = "Teams/TeamData", order = 1)]
public class TeamData : ScriptableObject {
    
    public string Name;
    public Sprite StandSprite;
    public Sprite JumpSprite;
    public Sprite LogoSprite;
    
    [Header("Colors")]
    public Color GlowColor;
    public Color IconColor;
    public Color ArrowColorBase;
    public Color ArrowColorBright;

    [Header("Team Links")]
    public TeamData PrevTeam;
    public TeamData NextTeam;

}
