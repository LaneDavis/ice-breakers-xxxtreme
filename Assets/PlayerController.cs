﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerController : MonoBehaviour {
    public float MoveSpeed;
    public int PlayerNum;
    private Player player;
    public Transform SpriteTransform;
    public Vector2 DefaultThrow;
    public SoundCall ThrowSound;

    public ParticleSystem ScamperParticles;
    public float ScamperEmitMultiplier;
    private bool touchedGroundThisFrame;
    private float distMovedThisFrame;
    public SpriteRenderer BodySprite;
    public Sprite StandSprite;
    public Sprite JumpSprite;

    [Header("Snowball Throwing")]
    public AnimationCurve ThrowStrengthByHeldTime;
    public float ThrowCooldown;
    private float ThrowTimer; 
    public GameObject SnowballPrefab;
    private bool isHoldingThrow;
    private float throwHeldTime;
    public SpriteRenderer HoldSpriteRenderer;
    public Sprite HoldSpriteStand;
    public Sprite HoldSpriteJump;
    private float HoldProgress => throwHeldTime / ThrowStrengthByHeldTime.LastFrame().time;
    public AnimationCurve HoldGlowOpacByProgress;
    public AnimationCurve HoldGlowOpacFinishedPulse;
    private bool hasDoneHoldFinishedPulseThisHold;
    public SoundCall HoldFinishedSound;
    public float TimeBetweenBonusThrows;
    private Vector2 firstBonusThrowVector;
    private Vector2 secondBonusThrowVector;
    public SoundCall ChargeUpSound;
    public AnimationCurve ChargeUpSoundVolumeByProgress;
    private bool hasDoneChargeUpSoundThisHold;

    [Header("After Image")]
    public GameObject AfterImageTemplate;
    public float AfterImageCooldown = 0.5f;
    private float afterImageCooldownLeft;

    [Header("Animations")]
    public FakeCharacterAnimations CharacterAnimations;
    
    [Header("Jump")]
    public int JumpCount = 2;
    public AnimationCurve JumpStrengthOverTime;
    public float JumpStrengthInitial = 1.5f;
    private int JumpsLeft;
    public float GravNormal = 8f;
    public float GravJumping = 5f;
    public SoundCall JumpSound;

    [Header("Hats")]
    public SpriteRenderer HatSR;
    public List<Sprite> HatSprites;
    public Sprite CrownSprite;

    [Header("Death Splash")]
    public GameObject DeathSplash;

    [HideInInspector] public Color TeamColor;

    private bool firstUpdate = true;

    [Header("Shadow Casters")]
    public GameObject ShadowCasterPrefab;
    
    // Use this for initialization
    void Start () {
        JumpsLeft = JumpCount;
        player = ReInput.players.GetPlayer(PlayerNum);
        
        // Initialize ShadowCasters (provide screen-wrap shadows).
        GameObject LeftCaster = Instantiate(ShadowCasterPrefab);
        GameObject RightCaster = Instantiate(ShadowCasterPrefab);
        LeftCaster.GetComponent<ShadowCaster>().SetTrackedObject(CharacterAnimations.transform, -1f);
        RightCaster.GetComponent<ShadowCaster>().SetTrackedObject(CharacterAnimations.transform, 1f);
    }

    public void SetTeamColor (Color color) {
        TeamColor = color;
        var main = ScamperParticles.main;
        main.startColor = new ParticleSystem.MinMaxGradient(color, Color.white); // random between 2 colors
    }

    public void SetHat(Sprite hatSprite) {
        if (PlayerManager.Instance.PlayerOnWinningTeam(this)) {
            HatSR.sprite = CrownSprite;
        }
        else {
            HatSR.sprite = hatSprite;
        }
    }

    private void FirstUpdate() {
        firstUpdate = false;
    }

    // Update is called once per frame
    void Update() {

        if (firstUpdate) {
            FirstUpdate();
        }

        // Jump
        UpdateJump();

        // Throw
        UpdateThrow();

        // Move
        float moveX = player.GetAxis("MoveX");
        transform.Translate(Vector2.left * moveX * -MoveSpeed);
        distMovedThisFrame = Mathf.Abs(moveX * MoveSpeed);
        SpriteTransform.localScale = moveX > 0 ? new Vector3(-1f, 1f, 1f) :
            moveX < 0 ? new Vector3(1f, 1f, 1f) :
            SpriteTransform.localScale;
        HoldSpriteRenderer.transform.localScale = moveX > 0 ? new Vector3(-1f, 1f, 1f) :
            moveX < 0 ? new Vector3(1f, 1f, 1f) :
            HoldSpriteRenderer.transform.localScale;
        CharacterAnimations.WalkAnimation(moveX);
    }

    private bool isJumping;
    private float jumpTimer = 0f;
    
    private void UpdateJump() {
        
        // Start Jump
        if (player.GetButtonDown("Jump")) {
            if (JumpsLeft > 0) {
                SoundManager.Instance.PlaySound(JumpSound, gameObject);
                GetComponent<Rigidbody2D>().velocity = new Vector2(0f, JumpStrengthInitial);
                isJumping = true;
                jumpTimer = 0f;
                if (JumpsLeft == 1) {
                    if (SpriteTransform.localScale.x > 0) {
                        CharacterAnimations.DoubleJumpRightAnimation();
                    }
                    else {
                        CharacterAnimations.DoubleJumpLeftAnimation();
                    }
                } else {
                    CharacterAnimations.JumpAnimation();
                }
                JumpsLeft--;
            }
        }

        if (isJumping) {
            BodySprite.sprite = JumpSprite;
            HoldSpriteRenderer.sprite = HoldSpriteJump;
            GetComponent<Rigidbody2D>().velocity += new Vector2(0f, JumpStrengthOverTime.Evaluate(jumpTimer) * Time.deltaTime);
            GetComponent<Rigidbody2D>().gravityScale = GravJumping;
            jumpTimer += Time.deltaTime;
        }
        else {
            GetComponent<Rigidbody2D>().gravityScale = GravNormal;
        }

        if (player.GetButtonUp("Jump")) {
            isJumping = false;
        }

    }

    private void UpdateThrow() {
        ThrowTimer += Time.deltaTime;
        
        if (!isHoldingThrow && player.GetButton ("Throw") && ThrowTimer >= ThrowCooldown) {
            StartThrowHold();
        }

        if (isHoldingThrow) {
            ChargeUpSound.volume = ChargeUpSoundVolumeByProgress.Evaluate(HoldProgress);
            SoundManager.Instance.PlaySound(ChargeUpSound, gameObject);
            CharacterAnimations.FxHoldToggle.transform.localScale = new Vector3(HoldProgress * 1.1f + .45f, HoldProgress * 1.1f + .45f, HoldProgress * 1.1f + .45f);
            throwHeldTime += Time.deltaTime;
        }

        if (player.GetButtonUp("Throw") && isHoldingThrow) {
            if (HoldProgress >= 1f) {
                Vector2 input = new Vector2(player.GetAxis("ThrowX"), player.GetAxis("ThrowY"));
                firstBonusThrowVector = GeometryUtils.RotateVector(input, 7f);
                secondBonusThrowVector = GeometryUtils.RotateVector(input, -7f);
                ThreadingUtil.Instance.RunLater(() => Throw(firstBonusThrowVector), TimeBetweenBonusThrows);
                ThreadingUtil.Instance.RunLater(() => Throw(secondBonusThrowVector), TimeBetweenBonusThrows * 2);
            }
            Throw();
        }

        if (HoldProgress >= 1f && !hasDoneHoldFinishedPulseThisHold) {
            SoundManager.Instance.PlaySound(HoldFinishedSound, gameObject);
            CreateAfterImage();
            player.SetVibration(0, 1f);
            player.SetVibration(1, 1f);
            ThreadingUtil.Instance.RunLater(() => {
                player.SetVibration(0, 0f);
                player.SetVibration(1, 0f);
            }, 0.15f);
            hasDoneHoldFinishedPulseThisHold = true;
            CharacterAnimations.FxHoldToggle.ToggleAll(false);
        }
        
        float opac = HoldGlowOpacByProgress.Evaluate(HoldProgress);
        if (HoldProgress >= 1f) opac += HoldGlowOpacFinishedPulse.Evaluate(throwHeldTime - ThrowStrengthByHeldTime.LastFrame().time);
        HoldSpriteRenderer.color = new Color(1f, 1f, 1f, opac);

        if (HoldProgress >= 1f) {
            afterImageCooldownLeft -= Time.deltaTime;
            if (afterImageCooldownLeft <= 0f) {
                CreateAfterImage();
            }
        }
   
    }

    private void StartThrowHold() {
        hasDoneChargeUpSoundThisHold = false;
        hasDoneHoldFinishedPulseThisHold = false;
        CharacterAnimations.HoldStart();
        CharacterAnimations.FxHoldToggle.ToggleAll(true);
        isHoldingThrow = true;
        throwHeldTime = 0f;
    }

    private void Throw(Vector2 forcedInput = default(Vector2)) {
        isHoldingThrow = false;
        CharacterAnimations.FxHoldToggle.ToggleAll(false);
        CharacterAnimations.HoldStart();
        GameObject NewSnowball = Instantiate(SnowballPrefab, transform.position, Quaternion.identity, null);
        SoundManager.Instance.PlaySound(ThrowSound, gameObject);
        if (forcedInput == default(Vector2)) {
            if (Mathf.Abs(player.GetAxis("ThrowX")) < 0.05f && Mathf.Abs(player.GetAxis("ThrowY")) < 0.05f) {
                NewSnowball.GetComponent<Rigidbody2D>().velocity = new Vector2(DefaultThrow.x * SpriteTransform.localScale.x * -1, DefaultThrow.y) * ThrowStrengthByHeldTime.Evaluate(throwHeldTime);
            } else {
                Vector2 input = new Vector2(player.GetAxis("ThrowX"), player.GetAxis("ThrowY"));
                NewSnowball.GetComponent<Rigidbody2D>().velocity = input.normalized * Mathf.Clamp01(input.magnitude) * ThrowStrengthByHeldTime.Evaluate(throwHeldTime);
            }
        }
        else {
            NewSnowball.GetComponent<Rigidbody2D>().velocity = forcedInput.normalized * Mathf.Clamp01(forcedInput.magnitude) * ThrowStrengthByHeldTime.LastFrame().value;
        }
        foreach (ParticleSystem ps in NewSnowball.GetComponentsInChildren<ParticleSystem>()) {
            var main = ps.main;
            main.startColor = new ParticleSystem.MinMaxGradient(TeamColor, Color.white); // random between 2 colors
            ThrowTimer = 0;
        }

        throwHeldTime = 0f;
    }
    
    private void LateUpdate() {
        if (touchedGroundThisFrame) {
            BodySprite.sprite = StandSprite;
            HoldSpriteRenderer.sprite = HoldSpriteStand;
            ScamperParticles.emissionRate = ScamperEmitMultiplier * distMovedThisFrame;
        } else {
            ScamperParticles.emissionRate = 0f;
        }

        touchedGroundThisFrame = false;
    }

    // Reset Jumps
    public void ResetJumpsIfOnFloor(Collision2D collision) {
        if (collision.gameObject.name.Contains("Floor") && transform.position.y > collision.transform.position.y && !isJumping) {
            JumpsLeft = JumpCount;
        }
    }

    // Detect
    void OnCollisionEnter2D(Collision2D collision) {
        ResetJumpsIfOnFloor(collision);
        touchedGroundThisFrame = true;
    }

    // Detect
    void OnCollisionStay2D(Collision2D collision) {
        ResetJumpsIfOnFloor(collision);
        touchedGroundThisFrame = true;
    }

    private void CreateAfterImage() {
        afterImageCooldownLeft = AfterImageCooldown;
        GameObject newAfterImageObj = Instantiate(AfterImageTemplate, AfterImageTemplate.transform.position, AfterImageTemplate.transform.rotation, null);
        newAfterImageObj.SetActive(true);
        AfterImageController newAfterImageController = newAfterImageObj.GetComponent<AfterImageController>();
        newAfterImageController.SetScale(BodySprite.transform.localScale);
        if (BodySprite.sprite == JumpSprite) {
            newAfterImageController.InitJumping(TeamColor);
        }
        else {
            newAfterImageController.InitStanding(TeamColor);
        }
    }
}
