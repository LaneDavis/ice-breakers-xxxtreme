﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FxVariableValue : FxPackage {
   
    public float BaseValue = 1f;
    
    public ValueFactor.FactorStyle Style = ValueFactor.FactorStyle.Decay;
    
    // TIMED
    public float Timed_Duration = 0.1f;
    
    // DECAY
    public float Decay_DecayRate = 8f;
    public float Decay_MaxDuration = 10f;
    
    // CURVE
    public AnimationCurve Curve_Curve;
    
    // TOGGLE
    public bool Toggle_Easing;
    public AnimationCurve Toggle_EaseIn;
    public AnimationCurve Toggle_EaseOut;

    public ValueFactor MyValueFactor(float power = 1f, int ID = -1) {

        switch (Style) {
            case ValueFactor.FactorStyle.Timed:
                ValueFactor timedVf = new ValueFactor(BaseValue * power, Timed_Duration);
                timedVf.Power = power;
                return timedVf;
            case ValueFactor.FactorStyle.Decay:
                ValueFactor decayVf = new ValueFactor(BaseValue, Decay_DecayRate, Decay_MaxDuration);
                decayVf.Power = power;
                return decayVf;
            case ValueFactor.FactorStyle.Curve:
                ValueFactor curveVf = new ValueFactor(AnimationCurveUtilities.MultipliedCurve(Curve_Curve, power * BaseValue));
                curveVf.Power = power;
                return curveVf;
            case ValueFactor.FactorStyle.Toggle:
                ValueFactor toggleVf;
                if (!Toggle_Easing) toggleVf = new ValueFactor(BaseValue * power, ID);
                else toggleVf = new ValueFactor(BaseValue * power, ID, Toggle_EaseIn, Toggle_EaseOut);
                toggleVf.Power = power;
                return toggleVf;
        }

        return null;
    }
    
}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(FxVariableValue))]
public class FxVariableValueEditor : Editor {
    override public void OnInspectorGUI() {
        
        var myScript = target as FxVariableValue;
        
        List<string> excludedProperties = new List<string>();
        serializedObject.Update();

        if(myScript.Style != ValueFactor.FactorStyle.Timed){
            excludedProperties.Add("Timed_Duration");
        }
        
        if(myScript.Style != ValueFactor.FactorStyle.Decay){
            excludedProperties.Add("Decay_DecayRate");
            excludedProperties.Add("Decay_MaxDuration");
        }
        
        if(myScript.Style != ValueFactor.FactorStyle.Curve){
            excludedProperties.Add("Curve_Curve");
        }
        else {
            excludedProperties.Add("BaseValue");
        }
        
        if(myScript.Style != ValueFactor.FactorStyle.Toggle){
            excludedProperties.Add("Toggle_Easing");
        }

        if (myScript.Style != ValueFactor.FactorStyle.Toggle || !myScript.Toggle_Easing) {
            excludedProperties.Add("Toggle_EaseIn");
            excludedProperties.Add("Toggle_EaseOut");
        }

        DrawPropertiesExcluding(serializedObject, excludedProperties.ToArray());
        serializedObject.ApplyModifiedProperties();

    }
}
#endif
