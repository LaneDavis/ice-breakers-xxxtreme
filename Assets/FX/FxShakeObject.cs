﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Lets an FX Package apply translational and rotational shake to an object.
/// </summary>
public class FxShakeObject : FxVariableValue {

	//FACTOR ATTRIBUTES
	[Header("Translate")]
	[Range(0F, 100f)] public float TranslationMultiplier = 5f;
	[Header("Rotate")]
	[Range(0F, 100f)] public float RotationMultiplier;
	public float PerlinSpeed = 10f;
	public AnimationCurve ValueCurve = new AnimationCurve (new Keyframe (0f, 1f), new Keyframe (1f, 1f)); 

	//ANATOMY
	private FxApplierTranslate applier;
	private FxApplierRotate rotApplier;

	public override void SetUp (GameObject newAnchor = null) {
		
		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;

		if (newAnchor.GetComponent<FxApplierTranslate>() != null) {
			applier = newAnchor.GetComponent<FxApplierTranslate>();
		} else {
			applier = newAnchor.AddComponent<FxApplierTranslate>();
		}
		
		if (newAnchor.GetComponent<FxApplierRotate>() != null) {
			rotApplier = newAnchor.GetComponent<FxApplierRotate>();
		} else {
			rotApplier = newAnchor.AddComponent<FxApplierRotate>();
		}
		applier.SetUp(newAnchor);
		rotApplier.SetUp(newAnchor);

	}

	private void Update () {

		//If toggled on, set the shake value based on the curve.
		if (ToggleState) {
			applier.SetShakeFactor(SfxId, MyValueFactor(lastPower * TranslationMultiplier, SfxId), PerlinSpeed);
			rotApplier.SetShakeFactor(SfxId, MyValueFactor(lastPower * TranslationMultiplier, SfxId), PerlinSpeed);
		}

	}

	public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {

		if (!toggleState) {
			applier.EndShakeFactor(SfxId);
			rotApplier.EndShakeFactor(SfxId);
		}
		base.Toggle(toggleState, newAnchor, power);
		
	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		applier.SetShakeFactor(SfxId, MyValueFactor(lastPower * TranslationMultiplier, SfxId), PerlinSpeed);
		rotApplier.SetShakeFactor(SfxId, MyValueFactor(lastPower * RotationMultiplier, SfxId), PerlinSpeed);
	}

}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(FxShakeObject))]
public class FxShakeObjectEditor : Editor {
	override public void OnInspectorGUI() {
        
		var myScript = target as FxShakeObject;
        
		List<string> excludedProperties = new List<string>();
		serializedObject.Update();

		if(myScript.Style != ValueFactor.FactorStyle.Timed){
			excludedProperties.Add("Timed_Duration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Decay){
			excludedProperties.Add("Decay_DecayRate");
			excludedProperties.Add("Decay_MaxDuration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Curve){
			excludedProperties.Add("Curve_Curve");
		}
		else {
			excludedProperties.Add("BaseValue");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Toggle){
			excludedProperties.Add("Toggle_Easing");
		}

		if (myScript.Style != ValueFactor.FactorStyle.Toggle || !myScript.Toggle_Easing) {
			excludedProperties.Add("Toggle_EaseIn");
			excludedProperties.Add("Toggle_EaseOut");
		}

		DrawPropertiesExcluding(serializedObject, excludedProperties.ToArray());
		serializedObject.ApplyModifiedProperties();

	}
}
#endif
