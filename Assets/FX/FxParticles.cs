﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FxParticles : FxPackage {
    
	public enum ParticleStyle { Worldspace, Canvas };
	public ParticleStyle Style = ParticleStyle.Worldspace;

	//ATTRIBUTES
	public bool RendersUnderAnchor = true;
	public bool AddsCanvas = true;
	public bool SimpleEmit = true;
	public int SortingOrder = 5000;

	//ANATOMY
	private readonly List<ParticleSystem> systemList = new List<ParticleSystem>();

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;
		systemList.Clear();
		foreach (ParticleSystem system in GetComponentsInChildren<ParticleSystem>()) {
			systemList.Add(system);
		}

		//Add canvas component to parent, pushing it to a higher rendering order.
		if (Style != ParticleStyle.Canvas) return;
		if (!RendersUnderAnchor) return;
		if (AddsCanvas && newAnchor.GetComponent<Canvas>() == null) {
			newAnchor.AddComponent<Canvas>();
		}
		newAnchor.GetComponent<Canvas>().overrideSorting = true;
		newAnchor.GetComponent<Canvas>().sortingOrder = SortingOrder;

	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		if (SimpleEmit) {
			foreach (var t in systemList) {
				t.Emit((int)(t.emission.rateOverTime.Evaluate(0f) * power));
			}
		} else {
			foreach (var t in systemList) {
				t.Play();
			}
		}
	}

	public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
		if (ToggleState != toggleState){
			base.Toggle(toggleState, newAnchor);
			foreach (var t in systemList){
				if (toggleState)
					t.Play();
				else
					t.Stop();
			}
		}
	}

}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(FxParticles))]
public class FxParticlesEditor : Editor {
	override public void OnInspectorGUI() {
        
		var myScript = target as FxParticles;
		List<string> excludedProperties = new List<string>();
		serializedObject.Update();

		if(myScript.Style != FxParticles.ParticleStyle.Worldspace){
			// No Excluded Factors.
		}
        
		if(myScript.Style != FxParticles.ParticleStyle.Canvas){
			excludedProperties.Add("RendersUnderAnchor");
			excludedProperties.Add("AddsCanvas");
			excludedProperties.Add("SortingOrder");
		}

		DrawPropertiesExcluding(serializedObject, excludedProperties.ToArray());
		serializedObject.ApplyModifiedProperties();

	}
}
#endif
