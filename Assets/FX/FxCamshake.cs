﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxCamshake : FxPackage {
    
    [Range(0f, 1f)] public float Value = 1f;
    
    public override void Trigger (GameObject newAnchor = null, float power = 1f) {
        base.Trigger(newAnchor);
        CamshakeManager.ApplyTrauma(Value);
    }

    private void Update() {
        if (ToggleState) {
            CamshakeManager.ApplyTrauma(Value);
        }
    }
    
}
