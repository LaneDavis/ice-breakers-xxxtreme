﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class ValueFactor {
    
    public enum FactorStyle { Decay, Timed, Curve, Toggle };
    public FactorStyle Style;
    public float Age;
    public float Power = 1f;
    private float _decayRate;
    private float _value;
    private float _duration;
    private AnimationCurve _curve;
    private AnimationCurve _toggleInCurve = new AnimationCurve(new Keyframe(0f, 1f));
    private AnimationCurve _toggleOutCurve = new AnimationCurve(new Keyframe(0f, 0f));
    public int ID = -1;
    private float endTime = -1f;
    private bool fading;
    
    public float Value {
        get {
            switch (Style) {
                case FactorStyle.Decay:
                case FactorStyle.Timed:
                    return _value * Power;
                case FactorStyle.Toggle:
                    return _value * Power * _toggleInCurve.Evaluate(Age) * (!fading ? 1f : 1f - _toggleOutCurve.Evaluate(endTime - Time.time));
                case FactorStyle.Curve:
                    return _curve.Evaluate(Age) * Power;
                default:
                    return 0f;
            }
        }
    }

    public bool ShouldBeDeleted => Age > _duration || 
                                   Style == FactorStyle.Decay && Mathf.Abs(_value) < 0.001f || 
                                   Style == FactorStyle.Toggle && endTime > 0f && Time.time > endTime;

    public ValueFactor(AnimationCurve curve) {
        Style = FactorStyle.Curve;
        _curve = curve;
        _duration = curve.LastFrame().time;
    }

    public ValueFactor(float value, float time) {
        Style = FactorStyle.Timed;
        _value = value;
        _duration = time;
    }

    public ValueFactor(float value, float decayRate, float maxDuration) {
        Style = FactorStyle.Decay;
        _value = value;
        _decayRate = decayRate;
        _duration = maxDuration;
    }

    public ValueFactor(float value, int ID, AnimationCurve inCurve = null, AnimationCurve outCurve = null) {
        Style = FactorStyle.Toggle;
        _value = value;
        _duration = Mathf.Infinity;
        if (inCurve != null) _toggleInCurve = inCurve;
        if (outCurve != null) _toggleOutCurve = outCurve;
        _toggleInCurve.preWrapMode = _toggleInCurve.postWrapMode = _toggleOutCurve.preWrapMode = _toggleOutCurve.postWrapMode = WrapMode.Clamp;
        this.ID = ID;
    }

    public void UpdateAge(bool scaledTime = true) {
        Age += scaledTime ? Time.deltaTime : Time.unscaledDeltaTime;
        if (Style == FactorStyle.Decay) {
            _value = MathUtilities.Decay(_value, scaledTime ? Time.deltaTime : Time.unscaledDeltaTime, _decayRate);
        }
    }

    public void Fade() {
        if (Style == FactorStyle.Toggle) {
            if (!fading) {
                endTime = Time.time + _toggleOutCurve.LastFrame().time;
                fading = true;
            }
        }
        else Debug.LogWarning("Fade called on non-toggle ValueFactor.");
    }

}
