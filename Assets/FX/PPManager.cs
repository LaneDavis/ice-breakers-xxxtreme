﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PPManager : MonoBehaviour {

    public static PPManager Instance;

    public enum Volume {
        Default,
        Death,
        Swing,
        GoodImpact,
        Respawn
    }

    public List<PPVolumeRef> Vols = new List<PPVolumeRef>();
    private List<PPFactor> ActiveFactors = new List<PPFactor>();

    private void Awake() {
        Instance = this;
    }

    public void AddFactor(Volume volume, ValueFactor valueFactor, int ID) {
        PPFactor f = FactorWithID(ID);
        if (f != null) {
            f.ValueFactor = valueFactor;
            f.ValueFactor.Age = 0f;
        }
        else {
            ActiveFactors.Add(new PPFactor(volume, valueFactor, ID));
        }
    }

    public void RemoveFactor(int ID) {
        for (int i = ActiveFactors.Count - 1; i >= 0; i--) {
            if (ActiveFactors[i].ID == ID) ActiveFactors[i].ValueFactor.Fade();
        }
    }

    private void Update() {
        
        // Find the value of the strongest factor overall.
        float maxStrength = 0f;
        if (ActiveFactors.Count > 0) maxStrength = ActiveFactors.Max(e => e.Value);
        else maxStrength = 1f;

        // Set each PostProcessVolume's weight to be the max among its factors, divided by maxStrength.
        foreach (PPVolumeRef vol in Vols) {
            List<PPFactor> factors = FactorsOfVol(vol);
            if (factors.Count == 0) vol.Ref.weight = 0f;
            else vol.Ref.weight = factors.Max(e => e.Value) / maxStrength;
        }
        
        // Age factors and remove outdated ones.
        for (int i = ActiveFactors.Count - 1; i >= 0; i--) {
            ActiveFactors[i].IncrementAge(Time.deltaTime);
            if (ActiveFactors[i].ShouldBeDeleted) ActiveFactors.RemoveAt(i);
        }
        
    }

    private List<PPFactor> FactorsOfVol(PPVolumeRef vol) {
        return ActiveFactors.Where(e => e.Volume == vol.Volume).ToList();
    }

    private PPFactor FactorWithID(int ID) {
        foreach (PPFactor f in ActiveFactors) if (f.ID == ID) return f;
        return null;
    }

}

[System.Serializable]
public class PPVolumeRef {

    public PPManager.Volume Volume;
    public PostProcessVolume Ref;

}

[System.Serializable]
public class PPFactor {

    public PPManager.Volume Volume;

    [HideInInspector] public int ID;
    public ValueFactor ValueFactor;
    public float Value => ValueFactor.Value;
    public bool ShouldBeDeleted => ValueFactor.ShouldBeDeleted;

    public PPFactor (PPManager.Volume volume, ValueFactor valueFactor, int ID) {
        Volume = volume;
        ValueFactor = valueFactor;
        this.ID = ID;
    }

    public void IncrementAge (float time) {
        ValueFactor.UpdateAge();
    }
    
}