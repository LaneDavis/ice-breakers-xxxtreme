﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Translates an object to an offset of a base position set on awake.
/// </summary>
public class FxApplierTranslate : FxApplier {
	
	private GameObject anchor;

	private readonly List<FxTranslateFactor> translateFactors = new List<FxTranslateFactor> ();
	private readonly List<FxFactor> shakeFactors = new List<FxFactor> ();

	//SMOOTH TRANSLATE
	private Vector3 basePosition = Vector3.zero;
	private Vector2 factorSum = Vector2.zero;
	
	float xPosPerlinSeed;
	float yPosPerlinSeed;

	public void SetUp (GameObject newAnchor = null) {
		if (newAnchor == null) return;
		anchor = newAnchor;
		basePosition = newAnchor.transform.localPosition;
		
		xPosPerlinSeed = Random.Range(0f, 100f);
		yPosPerlinSeed = Random.Range(100f, 200f);
	}

	public void SetTranslateFactor (int sfxId, Vector2 directionVector, ValueFactor valueFactor, bool loops) {

		FxTranslateFactor factor = (FxTranslateFactor)FactorWithId(sfxId);
		if(factor != null) {
			factor.DirectionVector = directionVector.normalized;
			factor.ValueFactor = valueFactor;
			factor.ValueFactor.Age = 0f;
		} else {
			factor = new FxTranslateFactor (sfxId, directionVector.normalized, valueFactor);
			translateFactors.Add(factor);
		}

	}

	public void SetShakeFactor (int sfxId, ValueFactor valueFactor, float perlinSpeed) {

		FxFactor factor = FactorWithId(sfxId);
		if(factor != null) {
			factor.ValueFactor = valueFactor;
		} else {
			factor = new FxFactor {
				SfxId = sfxId,
				ValueFactor = valueFactor,
				Speed = perlinSpeed,
			};
			shakeFactors.Add(factor);
		}

	}

	private void Update () {

		AgeFactors();
		ApplyTranslation();

	}

	private void ApplyTranslation () {

		if (anchor != null) {
			anchor.transform.localPosition = basePosition + (Vector3)TranslationSum() + new Vector3(0.5f - Mathf.PerlinNoise(Time.time * ShakePerlinSpeed() * 2f, xPosPerlinSeed), 0.5f - Mathf.PerlinNoise(Time.time * ShakePerlinSpeed() * 2f, yPosPerlinSeed)) * ShakeSum() * 4.5f;
		}

	}

	public Vector2 TranslationSum () {
		factorSum = Vector2.zero;
		foreach (FxTranslateFactor factor in translateFactors) {
			factorSum += factor.LastDisplacement;
		}
		return factorSum;
	}

	public void EndFactor (int sfxId) {
		foreach (FxTranslateFactor factor in translateFactors) {
			if (factor.SfxId == sfxId)
				factor.Deactivate();
		}
	}
	
	public void EndShakeFactor (int sfxId) {
		foreach (FxFactor factor in shakeFactors) {
			if (factor.SfxId == sfxId)
				factor.ValueFactor.Fade();
		}
	}

	public float ShakeSum () {
		float shakeSum = 0F;
		foreach (FxFactor factor in shakeFactors) {
			shakeSum += factor.Value;
		}
		return shakeSum;
	}

	public float ShakePerlinSpeed() {
		if (shakeFactors.Count == 0) return 0f;
		float perlinSum = 0f;
		foreach (FxFactor factor in shakeFactors) {
			perlinSum += factor.Speed;
		}
		return perlinSum / shakeFactors.Count;
	}

	override protected void AgeFactors () {
		FactorsToDelete.Clear();
		foreach (FxTranslateFactor factor in translateFactors) {
			factor.UpdateFactor();
			if (factor.GetShouldBeDeleted())
				FactorsToDelete.Add(factor);
		}
		foreach (var fxFactor in FactorsToDelete) {
			var factor = (FxTranslateFactor) fxFactor;
			translateFactors.Remove(factor);
		}

		foreach (FxFactor factor in shakeFactors) {
			factor.UpdateFactor();
			if (factor.GetShouldBeDeleted())
				FactorsToDelete.Add(factor);
		}
		foreach (FxFactor factor in FactorsToDelete) {
			shakeFactors.Remove(factor);
		}
	}

	protected override FxFactor FactorWithId (int id) {
		foreach (FxFactor factor in translateFactors) {
			if (factor.SfxId == id)
				return factor;
		}
		foreach (FxFactor factor in shakeFactors) {
			if (factor.SfxId == id)
				return factor;
		}
		return null;
	}

}

public class FxTranslateFactor : FxFactor {

	public AnimationCurve ValueCurve;
	public Vector2 DirectionVector;

	public Vector2 LastDisplacement = Vector2.zero;
	protected bool IsOn = true;

	public FxTranslateFactor (int sfxId, Vector2 directionVector, ValueFactor valueFactor) {
		SfxId = sfxId;
		ValueFactor = valueFactor;
		DirectionVector = directionVector;
	}

	public override void UpdateFactor () {
		
		base.UpdateFactor();
		
		if (IsOn) {
			LastDisplacement = DisplacementFromValue();
		} else {
			LastDisplacement = LastDisplacement.normalized * LastDisplacement.magnitude * Mathf.Exp(-8f * Time.deltaTime);	// TODO: Add adjustable smoothing in FxVariableValue (toggled).
		}
		
	}

	public void Deactivate () {
		IsOn = false;
	}

	protected Vector2 DisplacementFromValue () {
		return DirectionVector * Value;
	}

}
