﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashController : MonoBehaviour {

    public float Duration = 5f;
    private float timer;

    private void Update() {
        timer += Time.deltaTime;
        if (timer > Duration) {
            Destroy(gameObject);
        }
    }

}
