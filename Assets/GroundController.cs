﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GroundController : MonoBehaviour {

    public int numBlocksToMake;
    public GameObject BurstParticle;
    public SoundCall BreakSound;

    public GameObject SelfPrefab;

    // Die Ground Die
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer.ToString() == "10")
        {
            CamshakeManager.ApplyTrauma(0.05f);
            SoundManager.thisSoundManager.PlaySound(BreakSound, gameObject);
            BurstParticle.GetComponent<ParticleSystem>().Emit(40);
            BurstParticle.transform.SetParent(null);
            Destroy(gameObject);
        }
    }

    public void MakeSomeBlocks () {
        for (int i = 1; i < numBlocksToMake; i++) {
            GameObject newBlock = Instantiate(gameObject, (Vector2)transform.position + Vector2.right * 2f * i, Quaternion.identity, transform.parent);
        }
    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(GroundController))]
public class GroundControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GroundController groundController = (GroundController)target;
        if (GUILayout.Button("Make Some Blocks"))
            groundController.MakeSomeBlocks();
    }
}
#endif